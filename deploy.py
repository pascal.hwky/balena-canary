"""
Balena canary deployment
Copyright (C) 2020  Hawkeye Recognition

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import time
import os
import sys
import json

from termcolor import colored
import balena

# Duration to sleep when release is still building
RUNNING_SLEEP_PERIOD = 60  # seconds
# Tag key containing stage numbers to match
TAG_KEY = 'rollout-stage'

# Helper functions
def error(m):
    print(colored(m, 'red'))
    sys.exit(-1)

def warning(m):
    print(colored(m, 'yellow'))

# User input
parser = argparse.ArgumentParser(description="""
    Canary deployment on Balena. Sets all Balena devices for which the tag "rollout-stage"
    matches the stage number to the release specified in the release file.
    """)
parser.add_argument('--release', type=str, required=True,
    help="Path to JSON file containg release information. Created by running e.g. `balena push <APP_NAME> -d > release.json`.")
parser.add_argument('--stage', type=int, required=True,
    help="The stage number for which the release in the release file should be deployed.")
parser.add_argument('--num-stages', type=int, required=True,
    help="The total number of stages. After the last stage, the application release policy will be updated.")
parser.add_argument('--balena-api-key', type=str,
    help="API key for your Balena account.", default=None)
parser.add_argument('--balena-app-name', type=str,
    help="Target application name.", default=None)
args = parser.parse_args()

release_file = args.release
stage = args.stage
num_stages = args.num_stages

if not args.balena_api_key:
    balena_api_key = os.environ['BALENA_API_KEY']
else:
    balena_api_key = args.balena_api_key
if not args.balena_app_name:
    app_name = os.environ['BALENA_APP_NAME']
else:
    app_name = args.balena_app_name

# Login to Balena
balena_client = balena.Balena()
balena_client.auth.login_with_token(balena_api_key)

# Get release ID from file
with open(release_file) as f:
    d = json.load(f)
release_id = d['releaseId']
# Provided API key may be invalid here
try:
    release_info = balena_client.models.release.get(release_id)
except balena.exceptions.ReleaseNotFound:
    error(f"Failed to find release {release_id}. Check whether the provided API key is valid.")

# Wait until release has finished building
while release_info['status'] == 'running':
    warning(f"Release {release_id} is still building. Sleeping for {RUNNING_SLEEP_PERIOD} seconds.")
    time.sleep(RUNNING_SLEEP_PERIOD)
    release_info = balena_client.models.release.get(release_id)
if release_info['status'] != 'success':
    error(f"Release {release_id} was unsuccessful. Please check build logs.")
release_hash = release_info['commit']

# Set devices in specified stage to relase ID
devices = balena_client.models.device.get_all_by_application(app_name)
if devices is None:
    error(f"No devices found in application {app_name}.")
print(f"Pinning devices in stage {stage} to release {release_id}")
updated_devices = list()
for idx, device in enumerate(devices):
    uuid = device['uuid']
    name = device['device_name']
    tags = balena_client.models.tag.device.get_all_by_device(uuid)
    tag = [t for t in tags if t['tag_key'] == TAG_KEY]
    if len(tag) > 0:
        device_stage = int(tag[0]['value'])
        if device_stage == stage:
            updated_devices.append(name)
            balena_client.models.device.set_to_release_by_id(uuid, release_id)
        elif device_stage > num_stages or device_stage <= 0:
            warning(f"Device {name} has tag {TAG_KEY} set to {device_stage}, should be from 1 to {num_stages}.")
    else:
        warning(f"Device {name} has no tag {TAG_KEY}.")
    completion = int(idx / len(devices) * 100)
    print(f"Completion: {completion}%", end="\r", flush=True)

# Give summary
print(f"Pinned {len(updated_devices)} devices to release {release_id}")

# Update application release policy after last stage
if stage == num_stages:
    print(f"Finished final stage (number {stage}). Updating application release policy.")
    app_info = balena_client.models.application.get(app_name)
    app_id = app_info['id']
    balena_client.models.application.set_to_release(app_id, release_hash)
